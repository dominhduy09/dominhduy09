<h1 align="center">Hi 👋, I'm Do Minh Duy</h1>
<p align="center">
<a><img src="https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&color=198E19&random=false&width=435&lines=I+am+into+Cybersecurity+and+Coding." alt="Typing SVG" /></a>
</p>

<img align="right" alt="Coding" width="430" src="https://images.squarespace-cdn.com/content/v1/5769fc401b631bab1addb2ab/1541580611624-TE64QGKRJG8SWAIUS7NS/coding-freak.gif">

- 🌱 I am learning **Computer Science** 

- 🤝 My bio: **https://bio.link/dmduy**

- 👨‍💻 My projects are available at: **https://github.com/dominhduy09** & **https://replit.com/@dominhduy** & **https://glitch.com/@dominhduy09**

- 📝 My blogs are available at: **https://dominhduy.blogspot.com** & **https://hackmd.io/@dominhduy** & **https://www.wattpad.com/user/user57213391**

- 📫 How to reach me: **dominhduy09@gmail.com** 

- ⚡ Fun fact: **I speak more than 2 languages (Vietnamese, English)** & I tend to learn more

</br>

<a href="https://skillicons.dev">
  <img src="https://skillicons.dev/icons?i=github,vim,linux,c,cpp,html,css,js,py,docker,postman" />
</a>
